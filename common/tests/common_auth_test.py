from django.conf import settings

from common.tests.common_test_utils import MachineAuthorizationUtil


class CommonAuthTest(MachineAuthorizationUtil):
    # Inherit this class for add common auth tests in your tests

    def setUp(self):
        super().setUp()

    def client_generate_common_request(self):
        # Method for check basic auth for given url in
        # Implement this method in your own TestCase class for check auth
        raise NotImplementedError

    def client_generate_common_request(self):
        # Method for generate common request for check auth for machine
        # Implement this method in your own TestCase class for check auth
        raise NotImplementedError

    def test_with_expired_token_get_auth_error(self):
        self.machine_auth_token = {settings.MACHINE_AUTH_TOKEN_HEADER: self.generate_expired_token()}
        response = self.client_generate_common_request()

        expected_json = {'detail': 'Machine ExpiredSignatureError'}
        self.assertEqual(403, response.status_code)
        self.assertJSONEqual(response.content, expected_json)

    def test_with_wrong_signature_token_get_auth_error(self):
        self.machine_auth_token = {settings.MACHINE_AUTH_TOKEN_HEADER: self.generate_wrong_signature_token()}
        response = self.client_generate_common_request()
        expected_json = {'detail': 'Machine InvalidSignatureError'}

        self.assertEqual(403, response.status_code)
        self.assertJSONEqual(response.content, expected_json)

    def test_with_wrong_algorithm_token_get_auth_error(self):
        self.machine_auth_token = {settings.MACHINE_AUTH_TOKEN_HEADER: self.generate_wrong_algorithm_token()}
        response = self.client_generate_common_request()
        expected_json = {'detail': 'Machine InvalidAlgorithmError'}

        self.assertEqual(403, response.status_code)
        self.assertJSONEqual(response.content, expected_json)

    def test_with_payload_without_machine_id_get_auth_error(self):
        self.machine_auth_token = {settings.MACHINE_AUTH_TOKEN_HEADER: self.generate_token_without_machine_id()}
        response = self.client_generate_common_request()
        expected_json = {'detail': 'Machine id not found in token'}

        self.assertEqual(403, response.status_code)
        self.assertJSONEqual(response.content, expected_json)

    def test_with_payload_with_not_exist_machine_id_get_auth_error(self):
        self.machine_auth_token = {settings.MACHINE_AUTH_TOKEN_HEADER: self.generate_token_with_not_exist_machine_id()}
        response = self.client_generate_common_request()
        expected_json = {'detail': 'Machine not found'}

        self.assertEqual(403, response.status_code)
        self.assertJSONEqual(response.content, expected_json)