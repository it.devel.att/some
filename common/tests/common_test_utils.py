from datetime import datetime, timedelta
from django.conf import settings

import jwt


class MachineAuthorizationUtil:

    def common_token_data(self):
        return {
            'machine_id': self.machine.id,
            'exp': datetime.utcnow() + timedelta(minutes=10)
        }

    def generate_auth_token(self):
        return jwt.encode(
            self.common_token_data(),
            self.machine.token_signature,
            algorithm=settings.MACHINE_JWT_ALGORITHM,
        )

    def generate_token_with_not_exist_machine_id(self):
        data = self.common_token_data()
        data.update({'machine_id': 9999999999999999999999999})
        return jwt.encode(
            data,
            self.machine.token_signature,
            algorithm=settings.MACHINE_JWT_ALGORITHM,
        )

    def generate_token_without_machine_id(self):
        data = self.common_token_data()
        data.pop('machine_id')
        return jwt.encode(
            data,
            self.machine.token_signature,
            algorithm=settings.MACHINE_JWT_ALGORITHM,
        )

    def generate_expired_token(self):
        data = self.common_token_data()
        data.update({'exp': datetime.utcnow() - timedelta(minutes=1)})
        return jwt.encode(
            data,
            self.machine.token_signature,
            algorithm=settings.MACHINE_JWT_ALGORITHM,
        )

    def generate_wrong_signature_token(self):
        return jwt.encode(
            self.common_token_data(),
            "SOME_WRONG_SIGNATURE",
            algorithm=settings.MACHINE_JWT_ALGORITHM,
        )

    def generate_wrong_algorithm_token(self):
        return jwt.encode(
            self.common_token_data(),
            self.machine.token_signature,
            algorithm='HS512',
        )
