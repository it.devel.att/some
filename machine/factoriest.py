import secrets
from random import randrange
import factory
from factory import SubFactory

from machine.models import Machine, Product, ProductInput, ProductOutput, MachineState


class MachineFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Machine

    address = factory.Sequence(lambda n: 'street {}'.format(n))
    hook_url = factory.Sequence(lambda n: 'https://some-endpoint/{}'.format(n))
    token_signature = factory.Sequence(lambda _: secrets.token_urlsafe(32))


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    name = factory.Sequence(lambda n: 'product {}'.format(n))


class ProductInputFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProductInput

    machine = SubFactory(MachineFactory)
    product = SubFactory(ProductFactory)


class ProductOutputFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProductOutput

    machine = SubFactory(MachineFactory)
    product = SubFactory(ProductFactory)


class MachineStateFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MachineState

    machine = SubFactory(MachineFactory)
    product = SubFactory(ProductFactory)
    current_qty = factory.LazyAttribute(lambda _: randrange(21))
    lower_limit_of_qty = factory.LazyAttribute(lambda _: randrange(21))
