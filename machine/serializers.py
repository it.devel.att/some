from rest_framework import serializers


class MachineProductSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()
