from django.db import transaction
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from machine import auth
from machine.models import Product, ProductOutput
from machine.serializers import MachineProductSerializer


class MachineWithdrawProductView(APIView):
    authentication_classes = [
        auth.MachineAuthentication,
    ]

    def post(self, request):
        with transaction.atomic():
            machine = request.machine
            serializer = MachineProductSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            product_id = serializer.data.get('product_id')
            product = get_object_or_404(Product, pk=product_id)

            ProductOutput.objects.create(machine=machine, product=product)

            machine_product_state, created = machine.states.get_or_create(product=product)
            machine_product_state.current_qty -= 1
            machine_product_state.save()

        if machine_product_state.notification_required():
            machine_product_state.send_notification()
        return Response(status=200)
