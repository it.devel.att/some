from django.urls import path, include

from machine.views import MachineWithdrawProductView

urlpatterns = [
    path('machine/product/withdraw', MachineWithdrawProductView.as_view(), name='machine_product_withdraw'),
]
