from django.conf import settings
from rest_framework import authentication, exceptions
import jwt

from machine.models import Machine


class MachineAuthentication(authentication.BaseAuthentication):
    ALGORITHM = settings.MACHINE_JWT_ALGORITHM

    def authenticate(self, request):
        machine_auth_token = request.META.get('HTTP_X_MACHINE_AUTH_TOKEN')
        if not machine_auth_token:
            raise exceptions.AuthenticationFailed('Machine auth token not found in request')

        # First try to get machine without check token signature
        data = jwt.decode(machine_auth_token, options={"verify_signature": False}, algorithms=[self.ALGORITHM])
        machine_id = data.get('machine_id')
        if not machine_id:
            raise exceptions.AuthenticationFailed('Machine id not found in token')

        try:
            machine = Machine.objects.get(pk=machine_id)
        except Machine.DoesNotExist:
            raise exceptions.AuthenticationFailed('Machine not found')

        # After get machine check token signature by machine signature store in database
        try:
            jwt.decode(machine_auth_token, machine.token_signature, algorithms=[self.ALGORITHM])
        except jwt.InvalidSignatureError:
            raise exceptions.AuthenticationFailed('Machine InvalidSignatureError')
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed('Machine ExpiredSignatureError')
        except jwt.InvalidAlgorithmError:
            raise exceptions.AuthenticationFailed('Machine InvalidAlgorithmError')
        except jwt.DecodeError:
            raise exceptions.AuthenticationFailed('Machine DecodeError')

        request.machine = machine
        return (machine, None)
