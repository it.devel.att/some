from django.conf import settings
from django.urls import reverse
from rest_framework.test import APITestCase
from mock import patch

from common.tests.common_auth_test import CommonAuthTest
from machine.factoriest import MachineFactory, ProductFactory, MachineStateFactory
from machine.models import MachineState, ProductOutput


class TestMachineWithdrawProduct(APITestCase, CommonAuthTest):

    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('machine_product_withdraw')

    def setUp(self) -> None:
        super().setUp()
        self.machine = MachineFactory()
        self.product = ProductFactory()
        self.machine_auth_token = {settings.MACHINE_AUTH_TOKEN_HEADER: self.generate_auth_token()}

    def client_generate_common_request(self):
        # Implements client_generate_common_request for CommonAuthTest
        return self.client.post(
            self.url,
            data=self.get_request_data(),
            format='json',
            **self.machine_auth_token,
        )

    def get_request_data(self):
        # Implements get_request_data for CommonAuthTest
        return {'product_id': self.product.id}

    @patch('requests.post')
    def test_success_withdraw_product_from_machine(self, _):
        response = self.client_generate_common_request()

        machine_product_state = MachineState.objects.filter(product=self.product, machine=self.machine).first()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(machine_product_state)
        self.assertEqual(-1, machine_product_state.current_qty)

    @patch('requests.post')
    def test_check_machine_product_state_decrement_even_if_product_not_exist_in_state(self, _):
        response = self.client_generate_common_request()

        machine_product_state = MachineState.objects.filter(product=self.product, machine=self.machine).first()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(machine_product_state)
        self.assertEqual(-1, machine_product_state.current_qty)

    @patch('requests.post')
    def test_check_machine_product_state_decrement(self, _):
        current_product_qty = 10
        MachineStateFactory(product=self.product, machine=self.machine, current_qty=current_product_qty)
        response = self.client_generate_common_request()

        machine_product_state = MachineState.objects.filter(product=self.product, machine=self.machine).first()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(machine_product_state)
        self.assertEqual(current_product_qty - 1, machine_product_state.current_qty)

    @patch('requests.post')
    def test_check_product_output_was_created_when_product_withdraw(self, _):
        product_output = ProductOutput.objects.filter(machine=self.machine, product=self.product).last()
        self.assertIsNone(product_output)
        response = self.client_generate_common_request()

        product_output = ProductOutput.objects.filter(machine=self.machine, product=self.product).last()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(product_output)

    @patch('requests.post')
    def test_if_machine_product_state_current_qty_lower_then_define_send_request(self, request):
        MachineStateFactory(product=self.product, machine=self.machine, current_qty=10, lower_limit_of_qty=9)
        response = self.client_generate_common_request()

        machine_product_state = MachineState.objects.filter(product=self.product, machine=self.machine).first()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(machine_product_state)
        self.assertEqual(9, machine_product_state.current_qty)
        request_notification_data = {
            'machine_id': self.machine.id,
            'product_id': self.product.id,
            'current_qty': 9,
        }
        request.called_once_with(url=self.machine.hook_url, data=request_notification_data)

    @patch('requests.post')
    def test_not_send_notification_request_if_product_qty_in_machine_more_then_lower_limit(self, request):
        MachineStateFactory(product=self.product, machine=self.machine, current_qty=10, lower_limit_of_qty=8)
        response = self.client_generate_common_request()

        machine_product_state = MachineState.objects.filter(product=self.product, machine=self.machine).first()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(machine_product_state)
        self.assertEqual(9, machine_product_state.current_qty)
        request.assert_not_called()

    @patch('requests.post')
    def test_if_machine_state_current_qty_lower_then_define_but_machine_hook_url_not_define_not_send_req(self, request):
        self.machine.hook_url = ''
        self.machine.save()

        MachineStateFactory(product=self.product, machine=self.machine, current_qty=10, lower_limit_of_qty=9)
        response = self.client_generate_common_request()

        machine_product_state = MachineState.objects.filter(product=self.product, machine=self.machine).first()
        self.assertEqual(200, response.status_code)
        self.assertIsNotNone(machine_product_state)
        self.assertEqual(9, machine_product_state.current_qty)
        request.assert_not_called()
