import requests
from django.db import models
from django.utils.translation import gettext_lazy as _


class AbstractDateTimeModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Machine(AbstractDateTimeModel):
    address = models.TextField(
        verbose_name='Address',
    )
    # TODO If update in model, somehow need to update on vendor machine
    token_signature = models.TextField(
        blank=False,
        verbose_name=_('Token signature'),
        help_text=_('Using for authenticate machine'),
    )
    hook_url = models.URLField(
        blank=True,
        verbose_name=_('URL for send notification'),
        help_text=_('URL for send notification about product ends'),
    )

    class Meta:
        verbose_name = _('Machine')
        verbose_name_plural = _('Machines')


class Product(AbstractDateTimeModel):
    name = models.CharField(
        max_length=255,
        verbose_name=_('Name'),
    )

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class ProductOutput(AbstractDateTimeModel):
    machine = models.ForeignKey(
        Machine,
        verbose_name=_('Machine'),
        related_name='product_outputs',
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        related_name='machine_outputs',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = _('Product output')
        verbose_name_plural = _('Product outputs')


# For future logic of product incomes
class ProductInput(AbstractDateTimeModel):
    machine = models.ForeignKey(
        Machine,
        verbose_name=_('Machine'),
        related_name='product_inputs',
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        related_name='machine_inputs',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = _('Product input')
        verbose_name_plural = _('Product inputs')


class MachineState(AbstractDateTimeModel):
    machine = models.ForeignKey(
        Machine,
        verbose_name=_('Machine'),
        related_name='states',
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        related_name='machine_input',
        on_delete=models.CASCADE,
    )
    current_qty = models.IntegerField(
        verbose_name=_('Current quantity'),
        default=0,
    )
    lower_limit_of_qty = models.IntegerField(
        verbose_name=_('Lower limit of quantity'),
        help_text=_('Quantity for send notification about product ends'),
        default=0,
    )

    class Meta:
        unique_together = (
            'machine',
            'product',
        )
        verbose_name = _('Machine state')
        verbose_name_plural = _('Machines state')

    def notification_required(self):
        return self.current_qty <= self.lower_limit_of_qty and self.machine.hook_url

    def send_notification(self):
        try:
            requests.post(
                url=self.machine.hook_url,
                data={
                    'machine_id': self.machine_id,
                    'product_id': self.product_id,
                    'current_qty': self.current_qty,
                },
            )
        # TODO Bad practice
        except Exception:
            # TODO added some retries? Logging?
            pass
